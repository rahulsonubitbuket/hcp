// tslint:disable-next-line:no-shadowed-variable
import { ConfigModel } from '../core/interfaces/config';

// tslint:disable-next-line:no-shadowed-variable
export class MenuConfig implements ConfigModel {
	public config: any = {};

	constructor() {
		this.config = {
			aside: {
				self: {},
				items: [
					// {
					// 	title: 'Home',
					// 	desc: 'Home',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/home.png',
					// 	page: '/hub/home',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// 	//badge: {type: 'm-badge--danger', value: '2'},
					// },
					// {
					// 	title: 'Dashboard',
					// 	desc: 'Case Information',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/dashboard.png',
					// 	page: '/hub/dashboard',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// },
					// {
					// 	title: 'Intake',
					// 	desc: 'Enrollment Task',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/intake.png',
					// 	page: '/hub/intake',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// },
					
					// {
					// 	title: 'Cases',
					// 	desc: 'Cases Info',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/case.png',
					// 	page: '/hub/case/view',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst','Nurse']
					// },
					// {
					// 	title: 'Patients',
					// 	desc: 'Patient Info',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/patient.png',
					// 	page: '/hub/patient/view',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// },
					// {
					// 	title: 'HCP',
					// 	desc: 'HCP info',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/hcp.png',
					// 	page: '/hub/hcp/view',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// },
					// {
					// 	title: 'Prescription',
					// 	desc: 'Prescription Info',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/rx.png',
					// 	page: '/hub/prescription',
					// 	roleAllow: ['Admin','Lead-Analyst']
					// },
					// {
					// 	title: 'HCP\'s Locations',
					// 	desc: 'external sites',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/site.png',
					// 	page: '/hub/site/view',
					// 	roleAllow: ['Admin','Analyst','Lead-Analyst']
					// },{
					// 	title: 'Stores',
					// 	desc: 'Stores of Drugs',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/store.png',
					// 	page: '/hub/store',
					// 	roleAllow: ['Admin','Lead-Analyst']
					// },
					// {
					// 	title: 'Drugs',
					// 	desc: 'Drugs Info',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/drug.png',
					// 	page: '/hub/drug',
					// 	roleAllow: ['Admin','Lead-Analyst']
					// },
					// // {
					// // 	title: 'Inbox',
					// // 	desc: 'inbox message',
					// // 	root: true,
					// // 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/mail.png',
					// // 	page: '/message',
					// // },
					// // {
					// // 	title: 'Chat Rooms',
					// // 	desc: 'Patient care chats',
					// // 	root: true,
					// // 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/chat.png',
					// // 	page: '/chat',
					// // },
					// // {
					// // 	title: 'Calender',
					// // 	desc: 'scheduler',
					// // 	root: true,
					// // 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/calendar.png',
					// // 	page: '/calender',
					// //},
					// {
					// 	title: 'Help Center',
					// 	desc: 'Help Center for app',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/helpcenter.png',
					// 	page: '/hub/help',
					// 	roleAllow: ['Admin','Lead-Analyst']
					// },
					// {
					// 	title: 'Settings',
					// 	desc: 'Settings for site',
					// 	root: true,
					// 	icon: './assets/cassia/default/media/img/logo/sidebar-icon/service.png',
					// 	page: '/hub/setting',
					// 	roleAllow: ['Admin']
					// },
					{
						title: 'Home',
						desc: 'Home of HCP',
						root: true,
						icon: './assets/cassia/default/media/img/logo/sidebar-icon/home.png',
						page: '/hcp/home',
						roleAllow: ['Hcp']
					},
					{
						title: 'User',
						desc: 'User of HCP',
						root: true,
						icon: './assets/cassia/default/media/img/logo/sidebar-icon/patient.png',
						page: '/hcp/user',
						roleAllow: ['Hcp']
					}
					
				]
			}
		};
	}
}
