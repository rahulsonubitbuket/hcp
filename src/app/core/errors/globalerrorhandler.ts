import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { LoggingService } from './logging.service';
import { ErrorService } from './error.service';
import { NotificationService } from './notification.service';
import { AuthService } from '../../content/sharedModules/authorization/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private injector: Injector,private authenticationService: AuthService) { }

    handleError(error: Error | HttpErrorResponse) {

        const errorService = this.injector.get(ErrorService);
        const logger = this.injector.get(LoggingService);
        const notifier = this.injector.get(NotificationService);
        let router = this.injector.get(Router);

        let message;
        let stackTrace;

        if (error instanceof HttpErrorResponse) {
            // Server Error
            // service-error interceptor will handle 
            if (error.status === 401) {
                alert(error.status + ': ' + error.statusText + ': You are Unauthorized. Please login again.')
                // this.authenticationService.signOut();
              }
              else if (error.status === 403) {
                alert(error.status + ': ' + error.statusText + ': You are Unauthorized to do this task');
                router.navigate(['/hub/dashboard'])
              } else {
                message = errorService.getServerMessage(error);
                  stackTrace = errorService.getServerStack(error);
                  notifier.showError(stackTrace,error.status);
              }    
        } else {
            // Client Error
            message = errorService.getClientMessage(error);
            stackTrace = errorService.getClientStack(error);
            notifier.showError(stackTrace,'clientError');
        }

        // Always log errors
        logger.logError(message, stackTrace);

    }
}