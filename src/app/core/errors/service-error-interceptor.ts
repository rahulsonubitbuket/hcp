import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent, HttpRequest, HttpHandler,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { LoggingService } from './logging.service';
import { ErrorService } from './error.service';
import { NotificationService } from './notification.service';
import { Router } from '@angular/router';
import { AuthService } from '../../content/sharedModules/authorization/auth.service';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {

  constructor(private injector: Injector, private authenticationService: AuthService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {

        const errorService = this.injector.get(ErrorService);
        const logger = this.injector.get(LoggingService);
        const notifier = this.injector.get(NotificationService);
        let message;
        let stackTrace;

        if (err.status === 401) {
          alert(err.status + ': ' + err.statusText + ': You are Unauthorized. Please login again.')
          // this.authenticationService.signOut();
        }
        else if (err.status === 403) {
          alert(err.status + ': ' + err.statusText + ': You are Unauthorized to do this task');
          this.router.navigate(['/hub/']);
        } else {
          message = errorService.getServerMessage(err);
            stackTrace = errorService.getServerStack(err);
            notifier.showError(stackTrace,err.status);
          return throwError(err.error);
        }
      })
    );
  }
}