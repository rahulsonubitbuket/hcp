import { Injectable} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private errorMessage = new BehaviorSubject<any>('');
  errorLog = this.errorMessage.asObservable();
  
  constructor(public router:Router) { }
  
  showSuccess(message: string): void {
   // this.snackBar.open(message);
   this.router.navigate(['/error/' + message]);
  }
  
  showError(stack: string,status: any): void {
    this.updatedErrorMessage(stack)
   this.router.navigate(['/error/type/' + status]);
  }

  updatedErrorMessage(stack: any){
    this.errorMessage.next(stack);
  }
}