import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hcpSpecificSearch'
})
export class HcpSpecificSearchPipe implements PipeTransform {

  transform(items: any, filter?: any) : any {
    if (!items) { return null; }
    if (!filter) { return items; }
  
    // console.log(filter); // JSON containing list of all searchFields
    // console.log(items); // data
  
    var filterKeys = Object.keys(filter);
    // console.log(filterKeys); //list of all searchFields
    // console.log(items.filter);
    // args => filter
    // value =>items
  
    return items.filter(item =>
      filterKeys.reduce((x, keyName) => 
          (x && new RegExp(filter[keyName], 'gi').test(item[keyName])), true));
          // (x && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] == "", true));
  
  }
}
