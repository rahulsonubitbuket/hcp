import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuAsideDirective } from './directives/menu-aside.directive';
import { MenuAsideOffcanvasDirective } from './directives/menu-aside-offcanvas.directive';
import { ScrollTopDirective } from './directives/scroll-top.directive';
import { HeaderDirective } from './directives/header.directive';
import { MenuAsideToggleDirective } from './directives/menu-aside-toggle.directive';
import { QuickSidebarOffcanvasDirective } from './directives/quick-sidebar-offcanvas.directive';
import { FirstLetterPipe } from './pipes/first-letter.pipe';
import { TimeElapsedPipe } from './pipes/time-elapsed.pipe';
import { QuickSearchDirective } from './directives/quick-search.directive';
import { JoinPipe } from './pipes/join.pipe';
import { GetObjectPipe } from './pipes/get-object.pipe';
import { ConsoleLogPipe } from './pipes/console-log.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { PortletDirective } from './directives/portlet.directive';
import { PatientSpecificSearchPipe } from './pipes/patient-specific-search.pipe';
import { HcpSpecificSearchPipe } from './pipes/hcp-specific-search.pipe';
import { PayerSpecificSearchPipe } from './pipes/payer-specific-search.pipe';
import { PlanSpecificSearchPipe } from './pipes/plan-specific-search.pipe';
import { PayerAssociatedPlanSpecificSearchPipe } from './pipes/payer-associated-plan-specific-search.pipe';
import { PhoneMaskDirective } from './directives/phone-number.directive';

@NgModule({
	imports: [CommonModule],
	declarations: [
		// directives
		MenuAsideDirective,
		MenuAsideOffcanvasDirective,
		ScrollTopDirective,
		HeaderDirective,
		MenuAsideToggleDirective,
		QuickSidebarOffcanvasDirective,
		QuickSearchDirective,
		PortletDirective,
		// pipes
		FirstLetterPipe,
		TimeElapsedPipe,
		JoinPipe,
		GetObjectPipe,
		ConsoleLogPipe,
		SafePipe,
		// PhoneMaskDirective,
		// PatientSpecificSearchPipe,
		// HcpSpecificSearchPipe,
		// PayerSpecificSearchPipe,
		// PlanSpecificSearchPipe,
		// PayerAssociatedPlanSpecificSearchPipe
	],
	exports: [
		// directives
		MenuAsideDirective,
		MenuAsideOffcanvasDirective,
		ScrollTopDirective,
		HeaderDirective,
		MenuAsideToggleDirective,
		QuickSidebarOffcanvasDirective,
		QuickSearchDirective,
		PortletDirective,
		// PhoneMaskDirective,
		// pipes
		FirstLetterPipe,
		TimeElapsedPipe,
		JoinPipe,
		GetObjectPipe,
		ConsoleLogPipe,
		SafePipe
	],
	providers: []
})
export class CoreModule {}
