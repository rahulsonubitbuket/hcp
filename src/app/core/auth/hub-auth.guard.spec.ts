import { TestBed, async, inject } from '@angular/core/testing';

import { HubAuthGuard } from './hub-auth.guard';

describe('HubAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HubAuthGuard]
    });
  });

  it('should ...', inject([HubAuthGuard], (guard: HubAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
