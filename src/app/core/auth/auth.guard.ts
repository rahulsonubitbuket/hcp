import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
one: boolean = true;
  constructor(private router: Router) { }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    
    console.log('AuthWithRedirectGuard.canActivate');
    
    let user = sessionStorage.getItem('HCP');
    if (user) {
      return true
    }
    else{
      this.router.navigateByUrl('/authorize/signin');
      return false;
    }
  }
}
