import { TestBed, async, inject } from '@angular/core/testing';

import { HcpAuthGuard } from './hcp-auth.guard';

describe('HcpAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HcpAuthGuard]
    });
  });

  it('should ...', inject([HcpAuthGuard], (guard: HcpAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
