import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HcpAuthGuard implements CanActivateChild {

  hcpUsers = environment.hcpUsers;
  roles: any;

  constructor(private router: Router) { }

  async canActivateChild(): Promise<boolean> {
    
    this.roles = JSON.parse(sessionStorage.getItem('userRole'));
    for (let role in this.roles) {
      for (let allowRoles in this.hcpUsers) {
        if (this.hcpUsers[allowRoles] == this.roles[role]['name']) {
          return true;
        }
      }
    }
    return false;
  }
}
