import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HubAuthGaurd implements CanActivateChild {

  hubUsers = environment.hubUsers;
  roles: any;

  constructor(private router: Router) {
    console.log('hcp');
   }
  

  async canActivateChild(): Promise<boolean> {
    console.log('hcp');

    this.roles = JSON.parse(sessionStorage.getItem('userRole'));
    for (let role in this.roles) {
      console.log('for1');
      
      for (let allowRoles in this.hubUsers) {
        if (this.hubUsers[allowRoles] == this.roles[role]['name']) {
          return true;
        }
      }
    }
    return false;

  }
}



