import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReminderServiceService {
  
  baseUrl = environment.baseUrl;
  private reminderApiPath = this.baseUrl+'Reminder/getByUser';
  constructor(private http: HttpClient) { }

  getUserReminders() {
    return this.http.get(this.reminderApiPath).pipe(map(res => res));
  }

  getRemindersToDisplay() {
   return this.getUserReminders().pipe(map(reminders => {
      this.setRemindersToDisplay(reminders);
    return reminders;
    })
    );
  }

  setRemindersToDisplay(res) {
    for (const reminder of res) {
      if (new Date(reminder.reminder1).getTime() > new Date().getTime()) {
        const timeDiff = new Date(reminder.reminder1).getTime() - new Date().getTime();
        setTimeout(function () {
          Swal.fire({
            type: 'info',
            title: 'Reminder',
            text: `Case Id - ${reminder.caseId} Message - ${reminder.reminderMessage}`
          });
        }, timeDiff);
      }
      if (new Date(reminder.reminder2).getTime() > new Date().getTime()) {
        const timeDiff = new Date(reminder.reminder2).getTime() - new Date().getTime();
        setTimeout(function () {
          Swal.fire({
            type: 'info',
            title: 'Reminder',
            text: `Case Id - ${reminder.caseId} Message - ${reminder.reminderMessage}`
          });
        }, timeDiff);
      }
      if (new Date(reminder.reminder3).getTime() > new Date().getTime()) {
        const timeDiff = new Date(reminder.reminder3).getTime() - new Date().getTime();
        setTimeout(function () {
          Swal.fire({
            type: 'info',
            title: 'Reminder',
            text: `Case Id - ${reminder.caseId} Message - ${reminder.reminderMessage}`
          });
        }, timeDiff);
      }
    }
  }
}
