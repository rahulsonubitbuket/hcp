import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { AuthGuard } from '../../core/auth/auth.guard';
// import { SettingResolve } from '../hub/setting/setting.resolve';
import { HcpAuthGuard } from '../../core/auth/hcp-auth.guard';
import { HubAuthGaurd } from '../../core/auth/hub-auth.guard';

const routes: Routes = [

	{
		path: '',
		component: PagesComponent,
		children: [
			{
				path: 'header/actions',
				component: ActionComponent
			},
			{
				path: 'hcp',
				loadChildren: '../hcp/hcp.module#HcpModule'
			},
			{
				path: 'profile',
				component: ProfileComponent
			},
		]
	},
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class PagesRoutingModule {
}
