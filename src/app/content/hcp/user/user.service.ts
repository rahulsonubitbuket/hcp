import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.baseUrl;
  default: string = this.baseUrl;
  hcpUserURL: string = 'hcpUsers';

  constructor(private http: HttpClient) { }

  addHcpUser(userData: {}) {
    console.log(userData);
    return this.http.post(this.default + this.hcpUserURL, userData).pipe(map(res => res));
  }

}
