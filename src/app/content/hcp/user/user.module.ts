import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { RouterModule } from '@angular/router';
import { UserAddComponent } from './user-add/user-add.component';

@NgModule({
  declarations: [UserComponent, UserAddComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
			{
				path: '',
				component: UserComponent
      },
      {
				path: 'add',
				component: UserAddComponent
			}
		])
  ]
})
export class UserModule { }
