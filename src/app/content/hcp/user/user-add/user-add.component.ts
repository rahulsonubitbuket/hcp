import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'm-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  userForm: FormGroup;
  hide = true;

  constructor(private fb: FormBuilder, private userService: UserService) { }

  ngOnInit() {

    this.userForm = this.fb.group({
      
      name: new FormControl('', Validators.required),
      loginId: new FormControl('', Validators.required),
      emailId: new FormControl('', Validators.email),
      password: new FormControl('', Validators.required),
      active: new FormControl('', Validators.required),
      userType: new FormControl(''),
      assignedNpi: new FormControl(''),
      assignedLocation: new FormControl(''),

    })
    console.log(this.userForm.value);
  }

  onSubmit(){
    console.log(this.userForm.value);

    this.userService.addHcpUser(this.userForm.value).subscribe(res => {
      console.log(res);
      this.userForm.reset();
      
    });
  }

}
