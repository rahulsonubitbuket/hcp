import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { MaterialModule } from '../../../material.module';
import { RouterModule } from '@angular/router';
import { NgxDonutChartModule } from 'ngx-doughnut-chart';
import {RatingModule} from "ngx-rating";
import { DataTablesModule } from 'angular-datatables';
import { BarRatingModule } from "ngx-bar-rating";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    MaterialModule,
    NgxDonutChartModule,
    BarRatingModule,
    RatingModule,
    DataTablesModule,
    RouterModule.forChild([
			{
				path: '',
				component: HomeComponent
			}
		])
  ]
})
export class HomeModule { }
