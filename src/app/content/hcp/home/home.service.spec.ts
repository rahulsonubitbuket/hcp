import { TestBed } from '@angular/core/testing';

import { HomeService } from './home.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { MatSort } from '@angular/material';
import { pipe, Observable, observable } from 'rxjs';
import { Source } from 'webpack-sources';

describe('HomeService', () => {

  beforeEach(() => TestBed.configureTestingModule({    
      imports:[HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: HomeService = TestBed.get(HomeService);
    expect(service).toBeTruthy();
  });
});
