import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChartType } from 'chart.js';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { merge, Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { HomeService } from '../../hcp/home/home.service';

@Component({
  selector: 'm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  donutChartData = [
    {
      label: 'Fax Infusion',
      value: 10,
      color: 'lightblue',
    },
    {
      label: 'Fax Rx',
      value: 13,
      color: 'lightgreen',
    },
    {
      label: 'eScript Infusion',
      value: 5,
      color: 'lightpink',
    },
    {
      label: 'eScript RX',
      value: 20,
      color: '#DDBDF1',
    },
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLabels: string[] = ['Fax Infusion', 'Fax Rx', 'eScript Infusion', 'eScript RX'];
  public doughnutChartData: any[] = [[10, 13, 5, 20],];
  starsCount: any;
  columnsToDisplay: any[] = ['order-date','txn-date', 'patient', 'drug', 'doctor','doctor-facility', 'overall-sub-status', 'overall-state','referral-type', 'info-source', 'rating', 'hcp-comment', 'actions']
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any[] = [];
  referralStatus: any[] = [];
  observe: Observable<any>;
  resultsLength = 0;
  isLoadingResults = true;
  rate:number = 3;

  constructor(private homeService: HomeService, private ref: ChangeDetectorRef) { }

  ngOnInit() {
    // this.tableData = [
    //   { dateRecieved: '02/02/2019 12:00:00', patientName: 'Joe Woody', dob: '02/02/1980', drugName: 'Drug Name', typeOfOrigin: 'Fax', referralType: 'Infusion', store: 'Store Name', facilityName: 'Facility Name', ipStatus: 'Unable to start', ipSubStatus: 'Need Patient Details', roStatus: '', roSubStatus: '' },
    //   { dateRecieved: '02/02/2019 12:00:00', patientName: 'Joe Woody', dob: '02/02/1980', drugName: 'Drug Name', typeOfOrigin: 'eScript', referralType: 'RX', store: 'Store Name', facilityName: 'Facility Name', ipStatus: 'In Progress infusion-Appointment', ipSubStatus: 'Re-scheduling Appointmnet', roStatus: '', roSubStatus: '' },
    //   { dateRecieved: '02/02/2019 12:00:00', patientName: 'Joe Woody', dob: '02/02/1980', drugName: 'Drug Name', typeOfOrigin: 'Phone', referralType: 'Infusion', store: 'Store Name', facilityName: 'Facility Name', ipStatus: 'In Progress RX', ipSubStatus: 'PA Denied - Appeal Initiatedt', roStatus: 'PA Success', roSubStatus: 'PA Appeal Denied' },
    // ];
    // this.dataSource.data = this.tableData;
  }

  async ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.observe = this.homeService.getReferralStatus(this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
        })
      ).subscribe(referralStatus => {
        console.log(referralStatus);
        this.isLoadingResults = false;
        this.resultsLength = referralStatus['total'];
        this.referralStatus = referralStatus['results'];
        this.dataSource = this.referralStatus;
        console.log(this.dataSource);

        this.ref.detectChanges();
      });
  }


}
