import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  baseUrl = environment.baseUrl;
  default: string = this.baseUrl;
  referralStatusURL: string = 'Referralstatus/bySkipLimit/'

  constructor(private http: HttpClient) { }

  getReferralStatus(sort: any,order:any,pageIndex:any, pageSize: any) {
    return this.http.get(this.default + this.referralStatusURL + pageIndex*pageSize + '/' + pageSize).pipe(map(res => res || {}));
  }
}
