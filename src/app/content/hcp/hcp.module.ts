import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
			{
				path: 'home',
				loadChildren: '../hcp/home/home.module#HomeModule'
			},
			{
				path: 'user',
				loadChildren: '../hcp/user/user.module#UserModule'
			},

    ])
  ]
})
export class HcpModule { }
