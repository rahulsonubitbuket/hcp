import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { AfterViewInit, HostListener, NgZone } from '@angular/core';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';

import { animate, group, query, style, transition, trigger } from '@angular/animations';
import { timer } from 'rxjs/observable/timer';
import { take } from 'rxjs/operators';
import { NotificationService } from '../../../../core/errors/notification.service';

export type People = Person[];

export interface Person {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;
  company: Company;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface Geo {
  lat: string;
  lng: string;
}

@Component({
	selector: 'm-error-page',
	templateUrl: './error-page.component.html',
	styleUrls: ['./error-page.component.scss'],
	animations: [
		
	  ]
})
export class ErrorPageComponent implements OnInit {

	@HostBinding('class') classes: string = 'm-grid m-grid--hor m-grid--root m-page';

  @Input() errorType: any;
  errorMessage: any;

	constructor(private route: ActivatedRoute, private errorService: NotificationService) {
	}

	ngOnInit() {
		this.errorType = this.route.snapshot.params.type;
		this.errorService.errorLog.subscribe(message=>{
      this.errorMessage = message;
			// console.log(message);
		})
	}
}

