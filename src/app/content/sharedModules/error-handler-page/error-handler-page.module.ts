import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MaterialModule } from '../../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [ErrorPageComponent, PageNotFoundComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
			{
				path: '404',
				component: PageNotFoundComponent
      },
      {
        path: 'type/:type',
        component: ErrorPageComponent,
      }
		]),
  ]
})
export class ErrorHandlerPageModule { }
