import { Component, OnInit } from '@angular/core';
import { NgModel, FormGroup, FormBuilder, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Expression } from '@angular/compiler';

@Component({
  selector: 'm-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  signUpForm: FormGroup;
  entities: string[] = ['Analyst', 'Admin'];
  selectedEntity: string = '';
  isChecked: true;
  isButtonDisabled: boolean = false;
  signUpLoading: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {


}
}
