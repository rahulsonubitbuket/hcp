import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  serverUrl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }

  login(data) {
    // return sessionStorage.setItem('HCP', 'First')
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json'
    //   })
    // };

    // return this.http.post(this.serverUrl + 'api/CustomUsers/login', data, httpOptions)

    if (data.email === "admin@gmail.com" && data.password === "admin123") {
      return {
        code: 200,
        message: "Login Successful",
        data: data
      };
    } else {
      return {
        code: 503,
        message: "Invalid Credentials",
        data: null
      };
    }
  }
}