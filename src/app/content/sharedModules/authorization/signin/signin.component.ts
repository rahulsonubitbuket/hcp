import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'm-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent {

  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  error: {};
  loginError: string;
  response: any;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (localStorage.getItem('HCP') && localStorage.getItem('First')) {
      this.router.navigate(['/hcp/home']);
    }
  }

  login() {

    // this static code
    const login = this.authService.login(this.loginForm.value);
    if (login.code == 200) {
      this.router.navigate(['/hcp/home']);
    }

    // this dyanim code
    // this.authService.login(this.loginForm.value).subscribe((data) => {
    //   console.log("caleddd",data);
    //   this.router.navigate(['/hcp/home']);
    //   this.response = data;
    //   },
    //   (error) => {
    //     //debugger
    //     this.error = error}
    // );

  }

  signUp() {
    console.log("hey");
  }
}
