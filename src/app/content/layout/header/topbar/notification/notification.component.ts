import {
	Component,
	OnInit,
	HostBinding,
	HostListener,
	Input,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { ReminderServiceService } from '../../../../../core/services/reminder-service.service';

@Component({
	selector: 'm-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit {
	@HostBinding('class')
	// tslint:disable-next-line:max-line-length
	classes = 'm-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width';

	@HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';
	@HostBinding('attr.m-dropdown-persistent') attrDropdownPersisten = 'true';

	@Input() animateShake: any;
	@Input() animateBlink: any;
	reminderData: any;
	constructor(private reminderService: ReminderServiceService, private ref: ChangeDetectorRef) {
		// animate icon shake and dot blink
		this.reminderService.getRemindersToDisplay().subscribe(reminder => {
			this.reminderData = this.computeReminder(reminder);
			this.ref.detectChanges();
		})
		setInterval(() => {
			this.animateShake = 'm-animate-shake';
			this.animateBlink = 'm-animate-blink';
		}, 3000);
		setInterval(() => (this.animateShake = this.animateBlink = ''), 6000);
	}

	computeReminder(res) {
		let result = [];
		for (const reminder of res) {
			if (new Date(reminder.reminder1).getTime() > new Date().getTime()) {
				result.push({ caseId: reminder.caseId, reminderDate: new Date(reminder.reminder1), message: reminder.reminderMessage });
			}
			if (new Date(reminder.reminder2).getTime() > new Date().getTime()) {
				result.push({ caseId: reminder.caseId, reminderDate: new Date(reminder.reminder2), message: reminder.reminderMessage });
			}
			if (new Date(reminder.reminder3).getTime() > new Date().getTime()) {
				result.push({ caseId: reminder.caseId, reminderDate: new Date(reminder.reminder3), message: reminder.reminderMessage });
			}
		}
		result = result.slice(0, 5);
		return result;
	}

	ngOnInit(): void { }
}
