import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AuthGuard } from './core/auth/auth.guard';

const routes: Routes = [
	// {
	// 	path: '',
	// 	redirectTo: 'dashboard',
	// 	pathMatch: 'full',
	//   },
	{
		path: '',
		loadChildren: 'app/content/pages/pages.module#PagesModule',
		canActivate: [AuthGuard]
	},
	{
		path: 'authorize',
		loadChildren: './content/sharedModules/authorization/auth.module#AuthModule',
	},
	{
		path: 'error',
		loadChildren: './content/sharedModules/error-handler-page/error-handler-page.module#ErrorHandlerPageModule'
	},
	{
		path: '**',
		redirectTo: 'error/404',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
		NgxSpinnerModule
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
